from bs4 import BeautifulSoup
import requests
import json

res_set_rates = []

for j in range(1,3):
    response = requests.get(f'https://www.banki.ru/services/responses/bank/alfabank/?page={j}&is_countable=on')
    soup = BeautifulSoup(response.text, 'html.parser')
    script = soup.find_all("script", attrs={'type' : 'application/ld+json'})
    script_json = json.loads(script[0].get_text()
            .replace('\n', '')
            .replace('\t', '')
            .replace('\\', '')
            .replace("\r", "")
            .replace("\xa0", ""))
    for review in script_json['review']:
        review.pop('@type', None)
        review.pop('name', None)
        review['reviewRating'] = review['reviewRating']['ratingValue']
        review['datePublished'] = review['datePublished'].split(" ")[0]
    res_set_rates.extend(script_json['review'])
    print(f'Страница {j} готова')

print(res_set_rates)